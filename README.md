# Containing
- **one jupyter file** corresponding to the code for this project (python)
- **one research paper** created in result of this project
    - *in this file you will find all informations needed to understand the goal of the project, methods implemented and results*